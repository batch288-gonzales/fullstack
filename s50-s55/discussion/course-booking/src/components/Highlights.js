import{Container, Row, Col, Card} from 'react-bootstrap';


export default function Highlights(){

	return (

		<Container className = 'mt-5'>
			<Row>
				<Col className = "col-12 col-md-4 mt-3">
					<Card className = 'cardHighlight'>
					      <Card.Body>
					        <Card.Title>Learn From Home</Card.Title>
					        <Card.Text>
					          Lorem ipsum dolor sit amet. Eos sunt consectetur sit quod dolores aut deserunt fuga ut harum voluptatum ex obcaecati eligendi a possimus quibusdam et illum alias. Hic impedit nesciunt sed enim voluptatem qui doloremque quia et quis consequatur sit laborum recusandae sed excepturi assumenda eum delectus laboriosam.
					        </Card.Text>
					  
					      </Card.Body>
					    </Card>
					
				</Col>

				<Col className = "col-12 col-md-4 mt-3">
					<Card className = 'cardHighlight'>
					      <Card.Body>
					        <Card.Title>Study Now, Pay Later</Card.Title>
					        <Card.Text>
					          Ad animi voluptatem nam neque eius in voluptatibus rerum sit voluptatem fuga eum minus vero qui assumenda velit eum animi vero. Qui sunt rerum aut totam repellat ut natus iusto eum vero placeat eum voluptatum rerum a tempora quasi in eveniet quia! In laboriosam atque sed autem maiores aut quam amet?
					        </Card.Text>
					        
					      </Card.Body>
					    </Card>
					
				</Col>

				<Col className = "col-12 col-md-4 mt-3">
					<Card className = 'cardHighlight'>
					      <Card.Body>
					        <Card.Title>Be Part of our community</Card.Title>
					        <Card.Text>
					          Ab perferendis officiis sed natus quisquam et nulla repudiandae sit quaerat veniam aut quisquam veritatis 33 assumenda voluptate. Qui quia veniam vel voluptates aliquid et ratione quia eos ducimus voluptas et illum asperiores. Ut corrupti iusto sit enim galisum est autem dolores id inventore veritatis quo officiis voluptas.
					        </Card.Text>
					        
					      </Card.Body>
					    </Card>
					
				</Col>
			</Row>
		</Container>


		

		)
}