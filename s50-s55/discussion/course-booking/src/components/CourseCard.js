import{Container, Row, Col, Button, Card} from 'react-bootstrap';

import {useContext} from 'react';
import UserContext from '../UserContext.js'

// import the useState hook from react
import {useState, useEffect} from 'react';

import {Link} from 'react-router-dom';



export default function CourseCard(props){
	// consume the cotnent of the UserContext
	const {user} = useContext(UserContext);

	console.log(props.courseProp)
	// console.log(props.courseProp[0].id)

	// data destructuring
	const {_id, name, description, price} = props.courseProp;

	//Use the state hook for this component to be able to store its state
	//States are use to keep track of information related to individual components

		// Syntax: const [getter, setter] = useState(initiaGetterValue)

	const [count, setCount] = useState(0);
	// console.log(count)

	const [seats, setSeats] = useState(30);

	const [isDisabled, setIsDisabled] = useState(false);



	// setCount(2)
	// console.log(count)

	// this function will be envoke when the button enrol is clicked
	function enroll(){
		if (seats === 0) {
		    alert("No more seats.");
		    return;
		}else{
			setCount(count + 1);
			setSeats(seats - 1);
		}
	}

	// the function/side effect in our useEffect hook will invoke or run on the initial loading of our application and when there is/are changes on our dependencies.

	// Syntax:
		// useEffect (side effect, [depecdencies]);

	useEffect(() => {
		// console.log("Hi I'm from the useEffect!")
		if(seats === 0){
			setIsDisabled(true);
		}

	}, [seats]);

	

	return (

		<Container className = 'mt-5'>
			<Row>
				<Col className = "col-12 mt-3">
					<Card className = 'cardHighlight'>
					      <Card.Body>
					        <Card.Title>{props.courseProp.name}</Card.Title>
					        <Card.Subtitle className="mb-2 text-muted">Description:</Card.Subtitle>
					        <Card.Text>
					          {description}
					        </Card.Text>

					        <Card.Subtitle className="mb-2 text-muted">Price:</Card.Subtitle>
					        <Card.Text>
					          PhP {price}
					        </Card.Text>

					        <Card.Subtitle className="mb-2 text-muted">Enrollees:</Card.Subtitle>
					        <Card.Text>
					          {count}
					        </Card.Text>

					        {
					        	user !== null
					        	?
					        	<Button as = {Link} to = {`/courses/${_id}`}>Details</Button>
					        	:
					        	<Button as = {Link} to = '/login' >Login to enroll!</Button>

					        }

					        
					      </Card.Body>
					    </Card>
					
				</Col>

			</Row>
		</Container>


		

		)
}