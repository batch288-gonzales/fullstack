import {Container, Row, Col, Button, Form} from 'react-bootstrap';

import React, {useState, useEffect, useContext} from 'react';

import {Navigate, Link, useNavigate} from 'react-router-dom';

// import sweetalert
import Swal2 from 'sweetalert2';

import UserContext from '../UserContext.js';

import NotFound from '../components/NotFound.js';



export default function Login(){
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	
	// We are going to consume or use the UserContext
	const {user, setUser} = useContext(UserContext);

	// get.item method gets the value of the specified key from our local storage
	// const [user, setUser] = useState(localStorage.getItem('email'));	
	const [isDisabled, setIsDisabled] = useState(true);

	const navigate = useNavigate();

	const retrieveUserDetails = (token) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`, {
			method: 'GET',
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(data => {
			console.log(data);
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}

	
	useEffect(()=>{
		if (email !== '' && password !==''){

			setIsDisabled(false);

		}else{
			setIsDisabled(true);
		}


	}, [email, password]);

	
	function login(event){
		
		event.preventDefault()
		// console.log(user)

		// process a fetch request to the corresponding backend API
		// Syntax:
			// fetch(url, {options})
		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method : 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then (result => result.json())
		.then  (data => {
			if(data === false){
				// alert('There was an error encountered during the authentication. Please try again or try siging up first!')
				Swal2.fire({
					title : 'Authentication Failed!',
					icon : 'error',
					text : 'Check your login details and try again!'
				})
			}else{
				// alert('Login Successful!');
				localStorage.setItem('token', data.auth);
				retrieveUserDetails(data.auth);

				Swal2.fire({
					title : 'Login successful',
					icon : 'success',
					text : 'Welcome to Zuitt!'
				})

				navigate('/')

			}
		})




		// alert('You are now logged in!');
		// navigate('/');

		// Set the email of the authenticated user in the local storage
		// Syntax:
			// localStorage.setItem('propertyName', value)

		// localStorage.setItem('email', email);
		// setUser(localStorage.getItem('email'))

		setEmail('');
		setPassword('');
	}

if (user.id !== null) {

	// Render NotFound if user is already logged in
    return <NotFound />; 
  }

  return (


	(user.id === null) ?
	<Container className = 'mt-5'>
		<Row>
			<Col className = 'col-6 mx-auto'>
				<h1 className = 'text-center'>Login</h1>
				<Form onSubmit = {event => login(event)}>
				      <Form.Group className="mb-3" controlId="formBasicEmail">
				        <Form.Label>Email address</Form.Label>
				        <Form.Control 
				        	type="email" 
				        	value = {email}
				        	onChange = {event => {
				        		setEmail(event.target.value)
				        	}}
				        	placeholder="Enter email" />
				      </Form.Group>

				      <Form.Group className="mb-3" controlId="formBasicPassword">
				        <Form.Label>Password</Form.Label>
				        <Form.Control 
				        	type="password" 
				        	value = {password}
				        	onChange = {event => setPassword(event.target.value)}
				        	placeholder="Password" />
				      </Form.Group>

				      <p>No account yet? <Link to = '/register'>Sign up here</Link></p>

				      <Button variant="success" type="submit"
				      		disabled = {isDisabled}>
				        Login
				      </Button>
				    </Form>

			</Col>
		</Row>
	</Container>

	:

	<Navigate to = '/' />
	
	
	)
}