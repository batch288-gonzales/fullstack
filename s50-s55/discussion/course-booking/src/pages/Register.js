import {Container, Row, Col, Button, Form} from 'react-bootstrap';

import {useState, useEffect, useContext} from 'react';

import UserContext from '../UserContext.js'

import {Link, useNavigate} from 'react-router-dom';

import NotFound from '../components/NotFound.js';

import Swal2 from 'sweetalert2';



export default function Register(){
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [mobileNo, setMobileNo] = useState('');

	const {user, setUser} = useContext (UserContext);

	// we containe the useNavigate to navigate variable
	const navigate = useNavigate();

	const [isDisabled, setIsDisabled] = useState(true);

	//we have to use the useEffect in enabling the submit button
	useEffect(()=>{
		if (email !== '' && password1 !=='' && password2 !== '' && password1 === password2 && password1.length > 6 && mobileNo !== '' && mobileNo.length === 11){

			setIsDisabled(false);

		}else{
			setIsDisabled(true);
		}


	}, [firstName, lastName, email, password1, password2, mobileNo]);

	//function that will be triggered once we submit the form
	function register(event){
		//it prevents our pages to reload when submitting the forms
		event.preventDefault()

		/*localStorage.setItem('email', email);
		setUser(localStorage.getItem('email'));
		
		alert('Thank you for registering!');

		navigate('/login')*/

		fetch(`${process.env.REACT_APP_API_URL}/users/register`,{
			method: 'POST',
			headers: {

				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNo: mobileNo,
				password1: password1,
				password2: password2
			}),
		})
		.then(result => result.json())
		.then(data => {
		console.log(data);

			if (data === false) {
				Swal2.fire({
				    title: 'Duplicate email found!',
				    icon: 'error',
				    text: 'Please provide a different email.',
				    });
			} else {
				Swal2.fire({
				    title: 'Registration Successful',
				    icon: 'success',
				    text: 'Welcome to Zuitt!',
				    });

				navigate('/login');
			}
		});




		//clear input fields
		setFirstName('');
		setLastName('');
		setEmail('');
		setPassword1('');
		setPassword2('');
		setMobileNo('');
	}

if (user.id !== null) {

	// Render NotFound if user is already logged in
    return <NotFound />; 
  }

  return (
	<Container className = 'mt-5'>
		<Row>
			<Col className = 'col-6 mx-auto'>
				<h1 className = 'text-center'>Register</h1>
				<Form onSubmit = {event => register(event)}>
					  <Form.Group className="mb-3" controlId="formBasicFirstName">
					    <Form.Label>First Name</Form.Label>
					    <Form.Control 
					    	type="text" 
					    	value = {firstName}
					    	onChange = {event => {

					    		setFirstName(event.target.value)
					    	}}
					    	placeholder="Enter First Name" />
					  </Form.Group>

					  <Form.Group className="mb-3" controlId="formBasicLastName">
					    <Form.Label>Last Name</Form.Label>
					    <Form.Control 
					    	type="text" 
					    	value = {lastName}
					    	onChange = {event => {

					    		setLastName(event.target.value)
					    	}}
					    	placeholder="Enter Last Name" />
					  </Form.Group>

				      <Form.Group className="mb-3" controlId="formBasicEmail">
				        <Form.Label>Email address</Form.Label>
				        <Form.Control 
				        	type="email" 
				        	value = {email}
				        	onChange = {event => {

				        		setEmail(event.target.value)
				        	}}
				        	placeholder="Enter email" />

				        <Form.Text className="text-muted">
				            We'll never share your email with anyone else.
				        </Form.Text>
				      </Form.Group>

				      <Form.Group className="mb-3" controlId="formBasicMobileNo">
					    <Form.Label>Mobile Number</Form.Label>
					    <Form.Control 
					    	type="text" 
					    	value = {mobileNo}
					    	onChange = {event => {

					    		setMobileNo(event.target.value)
					    	}}
					    	placeholder="Enter Mobile Number" />
					  </Form.Group>

				      <Form.Group className="mb-3" controlId="formBasicPassword1">
				        <Form.Label>Password</Form.Label>
				        <Form.Control 
				        	type="password" 
				        	value = {password1}
				        	onChange = {event => setPassword1(event.target.value)}
				        	placeholder="Password" />
				      </Form.Group>

				      <Form.Group className="mb-3" controlId="formBasicPassword2">
				        <Form.Label>Confirm Password</Form.Label>
				        <Form.Control 
				        	type="password" 
				        	value = {password2}
				        	onChange = {event => setPassword2(event.target.value)}
				        	placeholder="Retype your nominated password" />
				      </Form.Group>

				      <p>Have an account already? <Link to = '/login'>Login here</Link></p>

				      <Button variant="primary" type="submit"
				      		disabled = {isDisabled}>
				        Submit
				      </Button>
				    </Form>

			</Col>
		</Row>
	</Container>

	)
}